extends KinematicBody2D

export var ball_speed = 800
export var shoot_rate = 0.9

var ball = preload("res://Scenes/Football.tscn")
var can_shoot = true

func _process(delta):
	if Input.is_action_pressed("shoot") and can_shoot:
		$AnimatedSprite.play("Idle")
		var ball_instance = ball.instance()
		ball_instance.position = $ShootPoint.get_global_position()
		ball_instance.rotation_degrees = rotation_degrees
		ball_instance.apply_impulse(Vector2(), Vector2(ball_speed, 0).rotated(rotation))
		get_tree().get_root().add_child(ball_instance)
		can_shoot = false
		yield(get_tree().create_timer(shoot_rate), "timeout")
		$AnimatedSprite.play("Move")
		can_shoot = true
