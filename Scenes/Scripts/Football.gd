extends RigidBody2D

func _on_Football_body_entered(body):
	if !body.is_in_group("shooter"):
		queue_free()
